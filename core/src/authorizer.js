import {jwkValidator, policyGenerator} from "./authorizers/index.js";
import log from 'lambda-log';

export const handler = async (event) => {
    if (!event.authorizationToken) {
        return 'Unauthorized';
    }

    const tokenParts = event.authorizationToken.split(' ');
    const tokenValue = tokenParts[1];

    if (!(tokenParts[0].toLowerCase() === 'bearer' && tokenValue)) {
        // no auth token!
        return 'Unauthorized';
    }

    try {
        let claims = await jwkValidator.validate(tokenValue);
        return policyGenerator.generate(claims.sub, 'Allow', event.methodArn, claims.email, claims['cognito:username']);
    } catch (err) {
        log.error('catch error. Invalid token');
        log.error(err);
        return 'Unauthorized';
    }
};

