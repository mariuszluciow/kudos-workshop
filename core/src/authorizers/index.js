import KeyLoader from "./key-loader.js";
import JwkValidator from "./jwk-validator.js";
import PolicyGenerator from "./policy-generator.js";

const {CLIENT_ID} = process.env
const keyLoader = new KeyLoader();
const jwkValidator = new JwkValidator(keyLoader, CLIENT_ID);
const policyGenerator = new PolicyGenerator();

export {jwkValidator, policyGenerator};
