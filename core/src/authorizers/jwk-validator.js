import jose from 'node-jose';

function JwkValidator(keyLoader, clientId) {

    const validate = async (token) => {
        // get the kid from the headers prior to verification
        const sections = token.split('.');
        let header = JSON.parse(jose.util.base64url.decode(sections[0]));
        let payload = JSON.parse(jose.util.base64url.decode(sections[1]));

        const key = await keyLoader.getKey(payload.iss, header.kid);
        const keystore = await jose.JWK.asKey(key);

        try {
            await jose.JWS.createVerify(keystore).verify(token);
        } catch (error) {
            console.log(error);
            throw 'Signature verification failed';
        }

        // Additionally, we can verify the token expiration
        const current_ts = Math.floor(new Date() / 1000);
        if (current_ts > payload.exp) {
            throw 'Token is expired';
        }
        // and the Audience (use claims.client_id if verifying an access token)
        if (payload.aud !== clientId) {
            throw 'Token was not issued for this audience';
        }
        return payload
    };

    return {
        validate
    };
};

export default JwkValidator;
