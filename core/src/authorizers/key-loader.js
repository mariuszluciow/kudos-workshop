import axios from 'axios';

function KeyLoader() {

    const keys = {};

    const getKey = async (issuer, kid) => {
        if (keys[kid]) {
            return keys[kid];
        }

        const keysUrl = `${issuer}/.well-known/jwks.json`;
        console.log(`Loading ${kid} key`);
        const response = await axios.get(keysUrl);
        const key = response.data.keys.find(key => kid === key.kid);
        keys[kid] = key;
        return key;
    };

    return {
        getKey
    };

};

export default KeyLoader;
