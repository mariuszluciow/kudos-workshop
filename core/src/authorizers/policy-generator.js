function PolicyGenerator() {

    const generate = (principalId, effect, resource, email, username) => {
        const authResponse = {};
        authResponse.principalId = principalId;
        if (effect && resource) {
            let resourceParts = resource.split('/');
            const policyDocument = {};
            policyDocument.Version = '2012-10-17';
            policyDocument.Statement = [];
            const statementOne = {};
            statementOne.Action = 'execute-api:Invoke';
            statementOne.Effect = effect;
            statementOne.Resource = resourceParts[0] + '/' + resourceParts[1] + '/*';
            policyDocument.Statement[0] = statementOne;
            authResponse.policyDocument = policyDocument;
        }
        if (email) {
            authResponse.context = {
                email,
                username
            };
        }
        return authResponse;
    };

    return {
        generate
    };

};

export default PolicyGenerator;
