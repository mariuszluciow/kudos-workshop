import AWS from 'aws-sdk';
import AWSXRay from 'aws-xray-sdk-core'

const AwsProvider = process.env.IS_OFFLINE ? AWS : AWSXRay.captureAWS(AWS);

const dynamoDb = new AwsProvider.DynamoDB.DocumentClient();

export {
    dynamoDb
};
