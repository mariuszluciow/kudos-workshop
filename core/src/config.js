
export const config = {
    "usersTable": process.env.USERS_TABLE,
    "kudosesTable": process.env.KUDOSES_TABLE,
    "kudosesTableRecipientIndex": process.env.KUDOSES_TABLE_RECIPIENT_INDEX,
}
