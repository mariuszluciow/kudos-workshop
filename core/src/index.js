import routes from './routes.js';
import serverless from 'serverless-http';

export const handler = async (event, context) => {
  return await serverless(routes)(event, context);
};
