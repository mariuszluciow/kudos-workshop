import {config} from '../config.js';
import {dynamoDb} from '../aws/index.js';

import KudosesService from "./kudoses-service.js";
import KudosesController from "./kudoses-controller.js";
import KudosesRouter from "./kudoses-router.js";
import {usersDao} from "../users/index.js";
import KudosesDao from "./kudoses-dao.js";

const kudosesDao = new KudosesDao(config, dynamoDb)
const kudosesService= new KudosesService(usersDao, kudosesDao);
const kudosesController = new KudosesController(kudosesService);
const kudosesRouter = new KudosesRouter(kudosesController);

export {
    kudosesRouter
};
