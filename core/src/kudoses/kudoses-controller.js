import log from 'lambda-log';
import {NO_KUDOSES_LEFT} from "./errors.js";

function KudosesController(kudosesService) {

    const award = async (req, res, next) => {
        try {
            log.info('Awarding kudos');
            res.json(await kudosesService.award(req.requestContext.authorizer.email, req.body));
        } catch (e) {
            if(e === NO_KUDOSES_LEFT) {
                log.info('User has no kudoses left')
                res.status(412).json('User has no kudoses left');
            } else {
                log.error('Failed to fetch user', e);
                next(e);
            }
        }
    };

    return {
        award
    };
}

export default KudosesController;
