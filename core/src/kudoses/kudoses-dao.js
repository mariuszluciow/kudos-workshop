function KudosesDao(config, dynamoDb) {

    const save = async (kudos) => {
        kudos.createdAt = new Date().getTime();
        kudos.updatedAt = new Date().getTime();
        let params = {
            TableName: config.kudosesTable,
            Item: kudos
        };
        await dynamoDb.put(params).promise();
        return kudos;
    };

    return {
        save
    };
}

export default KudosesDao;
