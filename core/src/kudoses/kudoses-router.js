import express from 'express';
import {validate, Joi} from 'express-validation';


function KudosesRouter(kudosesController) {
    const routes = express.Router({mergeParams: true});

    routes.post('', validate({
        body: Joi.object().keys({
            recipient: Joi.string().required().max(2000),
            message: Joi.string().required().max(2000)
        })
    }), kudosesController.award);

    return routes;
}

export default KudosesRouter;
