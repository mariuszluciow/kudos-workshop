import log from 'lambda-log';
import {NO_KUDOSES_LEFT} from "./errors.js";

function KudosesService(usersDao, kudosesDao) {

    const award = async (author, {recipient, message}) => {
        log.info(`Giving kudos to ${recipient}, from ${author} for ${message}`);

        let sender = await usersDao.get(author);
        if (sender.balance > 0) {
            try {
                sender.balance--;
                await usersDao.optimisticLockSave(sender)
            } catch (e) {
                log.error(e);
                throw e
            }

            return await kudosesDao.save({author, recipient, message})
        } else {
            throw NO_KUDOSES_LEFT
        }

    };

    return {
        award
    };
}

export default KudosesService;
