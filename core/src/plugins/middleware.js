import log from "lambda-log";
import {ValidationError} from 'express-validation';

const LoggingMiddleware = (req, res, next) => {
    const { method, url } = req;
    log.options.dynamicMeta = () => {
        return {
            timestamp: new Date().toISOString()
        };
    };
    log.info({method, url})
    next();
};

const ErrorResolver = function (err, req, res, next) {
    if (err instanceof ValidationError) {
        return res.status(err.statusCode).json(err)
    }

    return res.status(500).json(err)
};

export {LoggingMiddleware, ErrorResolver};
