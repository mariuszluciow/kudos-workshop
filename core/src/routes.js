import express from 'express';
import log from 'lambda-log';
import {LoggingMiddleware, ErrorResolver} from "./plugins/middleware.js";
import {usersRouter} from "./users/index.js";
import {kudosesRouter} from "./kudoses/index.js";

const routes = express();
routes.use(express.json());

routes.use(LoggingMiddleware);

routes.use('/api/kudoses', kudosesRouter);
routes.use('/api/users', usersRouter);

routes.use(ErrorResolver);

export default routes;
