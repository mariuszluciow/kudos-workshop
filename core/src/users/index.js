import {config} from '../config.js';
import {dynamoDb} from '../aws/index.js';
import UsersDao from "./users-dao.js";
import UsersService from "./users-service.js";
import UsersController from "./users-controller.js";
import UsersRouter from "./users-router.js";

const usersDao = new UsersDao(config, dynamoDb);
const linksService = new UsersService(usersDao);
const linksController = new UsersController(linksService);
const usersRouter = new UsersRouter(linksController);

export {
    usersRouter, usersDao
};
