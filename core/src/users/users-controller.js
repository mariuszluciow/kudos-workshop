import log from 'lambda-log';

function UsersController(usersService) {

    const getUser = async (req, res, next) => {
        try {
            log.info('Fetching user');
            res.json(await usersService.getUser(req.requestContext.authorizer));
        } catch (e) {
            log.error('Failed to fetch user', e);
            next(e);
        }
    };

    return {
        getUser
    };
}

export default UsersController;
