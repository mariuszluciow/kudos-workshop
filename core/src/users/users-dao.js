function UsersDao(config, dynamoDb) {

    const save = async (user) => {
        user.createdAt = new Date().getTime();
        user.updatedAt = new Date().getTime();
        let params = {
            TableName: config.usersTable,
            Item: user
        };
        await dynamoDb.put(params).promise();
        return user;
    };

    const get = async (id) => {
        let params = {
            TableName: config.usersTable,
            ConsistentRead: true,
            Key: {
                id
            }
        };
        let data = await dynamoDb.get(params).promise();
        return data.Item;
    };

    const optimisticLockSave = async (user) => {
        const lock = user.updatedAt;
        user.updatedAt = new Date().getTime();

        let params = {
            TableName: config.usersTable,
            ConditionExpression: "updatedAt = :updatedAt",
            Item: user,
            ExpressionAttributeValues: {
                ":updatedAt": lock
            },
        };
        return dynamoDb.put(params).promise()
            .then(() => user);
    };

    return {
        save,
        get,
        optimisticLockSave
    };
}

export default UsersDao;
