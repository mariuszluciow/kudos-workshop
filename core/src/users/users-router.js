import express from 'express';

function UsersRouter(usersController) {
    const routes = express.Router({mergeParams: true});

    routes.get('', usersController.getUser);

    return routes;
}

export default UsersRouter;
