

function UsersService(usersDao) {

    const getUser = async ({email}) => {
        let user = await usersDao.get(email);
        if (!user) {
            return await usersDao.save({
                id: email,
                balance: 3
            });
        }
        return user
    };

    return {
        getUser
    };
}

export default UsersService;
