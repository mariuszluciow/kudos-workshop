import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import "./App.css";
import Routes from "./Routes";
import {LinkContainer} from "react-router-bootstrap";
import {Auth, Hub} from "aws-amplify";

function App() {

    const [user, setUser] = useState(null);
    const [name, setName] = useState(null);

    useEffect(() => {
        const unsubscribe = Hub.listen("auth", ({payload: {event, data}}) => {
            switch (event) {
                case "signIn":
                    setUser(data);
                    break;
                case "signOut":
                    setUser(null);
                    break;
                default:
                    console.log(data)
            }
        });

        Auth.currentAuthenticatedUser()
            .then(currentUser => {
                setUser(currentUser)
                setName(currentUser.attributes.name)
            })
            .catch(() => console.log("Not signed in"));

        return unsubscribe;
    }, []);


    return <div className="App container">
        <Navbar collapseOnSelect>
            <Navbar.Brand>
                <Link to="/">Kudos</Link>
            </Navbar.Brand>
            <Navbar.Toggle/>
            <Navbar.Collapse>
                <Nav>
                    < LinkContainer to="/transfer">
                        <NavItem>Transfer</NavItem>
                    </LinkContainer>

                    {user ? <NavItem onClick={() => Auth.signOut()}>Logout {name}</NavItem> :
                        <LinkContainer to="/login">
                            <NavItem>Login</NavItem>
                        </LinkContainer>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
        <Routes/>
    </div>

}

export default App;
