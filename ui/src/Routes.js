import React from "react";
import {Route, Routes} from "react-router-dom";
import Home from "./containers/Home";
import Login from "./containers/Login";
import SignOut from "./containers/SignOut";
import Transfer from "./containers/Transfer";

const routes = () =>
    <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="/login" exact element={<Login />} />
        <Route path="/transfer" exact element={<Transfer />} />
        <Route path="/sign-out" exact element={<SignOut />} />
    </Routes>;

export default routes;