import {Auth} from 'aws-amplify';
import {CognitoHostedUIIdentityProvider} from '@aws-amplify/auth';


export default function Login() {

    return (
        <div className="container">

            <button onClick={() => Auth.federatedSignIn({provider: CognitoHostedUIIdentityProvider.Google})}>Open
                Google
            </button>

        </div>
    );
}
