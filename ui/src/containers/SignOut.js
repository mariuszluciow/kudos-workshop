
export default function SignOut() {

    return (
        <div className="container">

            <p>You are now logged out</p>

        </div>
    );
}
