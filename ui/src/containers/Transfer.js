import {useEffect, useState} from "react";
import {API} from "aws-amplify";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {useNavigate} from "react-router-dom";

export default function Transfer() {
    const [recipient, setRecipient] = useState("");
    const [message, setMessage] = useState("");
    const [balance, setBalance] = useState({});
    let navigate = useNavigate();

    useEffect(() => {
        const load = async () => {
            let balance = await API.get('Kudos', '/users');
            setBalance(balance)
        }

        load()
            .catch(console.error);

    }, [])

    async function Submit(e) {
        e.preventDefault();

        try {
            let response = await API.post('Kudos', '/kudoses', {body: {recipient, message}});
            alert(response);
            navigate("/");
        } catch (e) {
            console.log(e.response)
            alert(e)
        }
    }

    return (
        <div className="container">
            <h1>
                Kudoses left: {JSON.stringify(balance.balance)}
            </h1>

            <Form onSubmit={e => Submit(e)}>

                <Form.Group className="mb-3" controlId="formTo">
                    <Form.Label>To</Form.Label>
                    <Form.Control type="text" placeholder="To" value={recipient}
                                  onChange={(e) => setRecipient(e.target.value)}/>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formFor">
                    <Form.Label>For</Form.Label>
                    <Form.Control type="text" placeholder="For" value={message}
                                  onChange={(e) => setMessage(e.target.value)}/>
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>

        </div>
    );
}
