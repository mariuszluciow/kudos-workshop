import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter} from "react-router-dom";
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Amplify from "aws-amplify";
import config from "./config";

const isLocalhost = Boolean(
    window.location.hostname === "localhost" ||
    // [::1] is the IPv6 localhost address.
    window.location.hostname === "[::1]" ||
    // 127.0.0.1/8 is considered localhost for IPv4.
    window.location.hostname.match(
        /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
    )
);

Amplify.configure({
    Auth: {
        mandatorySignIn: true,
        region: config.region,
        userPoolId: config.userPoolId,
        userPoolWebClientId: config.clientId,
        oauth: {
            domain: config.appAuthWebDomain,
            redirectSignIn: isLocalhost ? 'http://localhost:8080' : config.appWebDomain,
            redirectSignOut: isLocalhost ? 'http://localhost:8080/sign-out' : config.appWebDomain + '/sign-out',
            responseType: "code"
        }
    },
    API: {
        endpoints: [
            {
                name: "Kudos",
                endpoint: '/api',
                custom_header: async () => {
                    return { Authorization: `Bearer ${(await Amplify.Auth.currentSession()).getIdToken().getJwtToken()}` }
                }
            }
        ]
    }
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
